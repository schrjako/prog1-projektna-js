import orodja
import time
import random


def dl(i, override=False):
    if orodja.shrani_spletno_stran(f"https://archiveofourown.org/works?commit="
                                   f"Sort+and+Filter&page={i+1}&work_search%5B"
                                   f"sort_column%5D=authors_to_sort_on&work_se"
                                   f"arch%5Bwords_from%5D=10000&work_search%5B"
                                   f"language_id%5D=en&tag_id=Harry+Potter+-+J"
                                   f"*d*+K*d*+Rowling", f"html/ao3_{i:04}.html",
                                   override):
        t=random.randint(1, 4)
        print(f'Cakam {t}"')
        time.sleep(t)


for i in range(3217):
    dl(i)
