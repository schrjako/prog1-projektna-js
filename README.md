# Harry Potter fanfictioni

Analiziral bom fanfiction-e (odslej ffic) iz Harry Potter fandom-a na spletni
strani [ao3](https://archiveofourown.org), ki so vsebujejo vsaj 10 000 besed in
so napisani v angleščini.

### Za vsak fanfiction bom zajel:

* Naslov in avtorja ter morebitno posvetitev dela,
* opis in tag-e zgodbe,
* število napisanih besed in poglavij (tako obstoječih kot pričakovanih, če je
	ta podatek naveden),
* morebitno pripadnost seriji,
* število komentarjev, 'Kudos'-ov (like/všeček), in hit-ov.

### Delovna vprašanja:

* Ali imajo zgodbe v zbirkah relativno več komentarjev kot samostojne zgodbe?
* Ali imajo zgodbe z manjšo povprečno dolžino poglavij več
	komentarjev/kudosev/zaznamkov kot tiste z večjo?
* Ali imajo novejše zgodbe (tiste,  ki so kasneje dobile zadnje poglavje/update)
	več zadetkov na poglavje kot starejše?
