import sys
import os
import argparse
import pandas as pd
import numpy as np
import re
import ast
from itertools import chain


def readff(fin):
    return pd.read_table(fin, index_col="id",
                         converters={'fandoms': ast.literal_eval,
                                     'req_tags': ast.literal_eval,
                                     'tags_commas': ast.literal_eval})


def create_tables(args):
    #   For Jupyter conosle:
    if False:
        with open("ffs.csv", newline="") as fin:
            ffs = readff(fin)
    if args.input == "-":
        with sys.stdin as fin:
            ffs = readff(fin)
    else:
        with open(args.input, newline="") as fin:
            ffs = readff(fin)
    authors = pd.DataFrame(data={"author": pd.DataFrame(ffs.groupby("author"))[0]})
    authors.to_csv("authors.csv", sep="\t")
    authors["author_id"] = range(authors.size)
    ffs = pd.merge(ffs, authors, how="left", on="author")
    ffs["author"] = ffs["author_id"]
    ffs.drop(columns=["author_id"], inplace=True)

    def to_many_to_many(ffs, arg):
        if False:
            arg='req_tags'
        tags_list = list(ffs[arg].map(set).agg(lambda s: set.union(*s)))
        tags = pd.DataFrame(data={"arg": tags_list})
        tag_to_id = {tag: i for i, tag in enumerate(tags_list)}
        m_to_m = pd.concat([pd.DataFrame(
            data={'ff': [ff_id] * len(l), 'tag': [tag_to_id[tag] for tag in l]}
            ) for ff_id, l in ffs[arg].items()
            ], copy=False, ignore_index=True)
        tags.to_csv(f"{arg}.csv", sep="\t")
        m_to_m.to_csv(f"{arg}_to_ff.csv", sep="\t")
        ffs.drop(columns=[arg], inplace=True)
        print('finished many to many')


    to_many_to_many(ffs, 'fandoms')
    to_many_to_many(ffs, 'req_tags')
    to_many_to_many(ffs, 'tags_commas')

    ffs.to_csv("ffs_processed.csv", sep="\t")

    ffs[ffs.collections != -1].index
    ffs.index

    #ffs
    #ffs.index
    #ffs[["req_tags"]]
    #ffs["summary"]
    #ffs["req_tags"].map(set)
    #ffs["tags_commas"].map(type)
    #ffs["req_tags"].map(set).agg(set.union)
    #ffs["req_tags"].map(set).agg(lambda s: set.union(*s))
    #ffs["tags_commas"]
    #ffs["tags_commas"].map(len).agg(sum)
    #len(ffs["tags_commas"].map(set).agg(lambda s: set.union(*s)))
    #len(ffs["req_tags"].map(set).agg(lambda s: set.union(*s)))
    #ffs["req_tags"].map(set).agg(lambda a: [print(a, type(a)), a][1])
    #ffs["tags_commas"].map(set).agg(lambda a: [print(a, type(a)), a][1])
    #type(ffs["req_tags"])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="Input csv file", default="ffs.csv")
    parser.add_argument("--force", "-f", help="Force new output", action="store_true")
    args = parser.parse_args()
    create_tables(args)
