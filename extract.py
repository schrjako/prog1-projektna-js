import argparse
import csv
import datetime
import os
import re
from multiprocessing import Process, Queue
from itertools import chain


get_ffs = re.compile(r'(<li[^\n]*role="article">.*?^</li>$)', re.M | re.S)
get_ff = re.compile(r'<li id="work_(?P<id>\d+)" .*?>.*?<h4 class="heading">\n'
                    r'\s*<a href="/works/(?P=id)">(?P<title>.*?)</a>\n'
                    r'(\s*by.*<a rel="author" .*?>(?P<author>.*?)</a>)?'
                    r'.*<h5 class="fandoms heading">(?P<fandoms_raw>.*?)</h5>'
                    r'.*<ul class="required-tags">(?P<required_tags_raw>.*?)</ul>'
                    r'.*<p class="datetime">(?P<last_updated>.*?)</p>'
                    r'.*<ul class="tags commas">(?P<tags_commas_raw>.*?)</ul>'
                    r'(.*<blockquote class="userstuff summary">(?P<summary>.*?)</blockquote>)?'
                    r'.*<dd class="words">(?P<words>.*?)</dd>'
                    r'.*<dd class="chapters">(<a.*?>)?(?P<chap_n>.*?)(</a>)?/(?P<chap_of>.*?)</dd>'
                    r'(.*?<dd class="collections"><a.*?>(?P<collections>\d+)</a></dd>)?'
                    r'(.*?<dd class="comments"><a.*?>(?P<comments>\d+)</a></dd>)?'
                    r'(.*?<dd class="kudos"><a.*?>(?P<kudos>\d+)</a></dd>)?'
                    r'(.*?<dd class="bookmarks"><a.*?>(?P<bookmarks>\d+)</a></dd>)?'
                    r'(.*?<dd class="hits">(<a.*?>)?(?P<hits>\d+)(</a>)?</dd>)?'
                    r'.*</li>'
                    , re.M | re.S)


get_fandoms = re.compile(r'<a class="tag" href=".*?">(.*?)</a>')
get_in_tag = re.compile(r'>([^<>]{2,}?)<')


def simplify_html(strin, out=False):
    strin = re.sub(simplify_html.match_html_tag, "", strin)
    if out:
        print('BEFORE:\n\n', repr(strin))
    strin = re.sub(simplify_html.match_multiple_whitespace, " ", strin)
    if out:
        print('AFTER:\n\n', repr(strin))
    return strin.strip()
simplify_html.match_html_tag = re.compile(r"<.*?>")
simplify_html.match_multiple_whitespace = re.compile(r"\s+", re.M)


def to_date(desc):
    m_to_num = {datetime.date(2022, i, 1).strftime("%b"): i for i in range(1, 13)}
    d, m, y = desc.split()
    return datetime.date(int(y), m_to_num[m], int(d))


def get_tags(raw):
    ans = set()
    for in_tag in get_in_tag.findall(raw):
        ans |= set(in_tag.split(', '))
    return list(ans)


def parse(q, qw):
    while not q.empty():
        i = q.get()
        fin = f'html/ao3_{i:04}.html'
        with open(fin) as f:
            text = f.read()
        occ = get_ffs.findall(text)
        attribute_error = []
        type_error = []
        for ff_raw in occ:
            try:
                ff_parsed = get_ff.match(ff_raw)
                if ff_parsed['id'] == '444518600000000000':
                    print(ff_parsed.groupdict())
                    simplify_html(ff_parsed['summary'], out=True)
                ff = {
                        'id': int(ff_parsed['id']),
                        'title': ff_parsed['title'],
                        'author': 'Anonymous' if ff_parsed['author'] is None else ff_parsed['author'],
                        'fandoms': get_fandoms.findall(ff_parsed['fandoms_raw']),
                        'req_tags': get_tags(ff_parsed['required_tags_raw']),
                        'last_updated': to_date(ff_parsed['last_updated']),
                        'tags_commas': get_tags(ff_parsed['tags_commas_raw']),
                        'summary': '' if ff_parsed['summary'] is None else simplify_html(ff_parsed['summary'].strip()),
                        'words': int(''.join(ff_parsed['words'].split(','))),
                        'chapters_written': int(ff_parsed['chap_n']),
                        'chapters_of': -1 if ff_parsed['chap_of'] == '?' else int(ff_parsed['chap_of']),
                        'collections': -1 if ff_parsed['collections'] is None else int(ff_parsed['collections']),
                        'comments': -1 if ff_parsed['comments'] is None else int(ff_parsed['comments']),
                        'kudos': -1 if ff_parsed['kudos'] is None else int(ff_parsed['kudos']),
                        'bookmarks': -1 if ff_parsed['bookmarks'] is None else int(ff_parsed['bookmarks']),
                        'hits': -1 if ff_parsed['hits'] is None else int(ff_parsed['hits']),
                        }
                if ff_parsed['id'] == '444518600000000000000':
                    print(ff)
                qw.put(ff)
            except AttributeError:
                print(f"{ff_raw}\nError!")
                attribute_error.append(ff_raw)
            except TypeError:
                print(f'{filename}: TypeError\n[{ff_parsed}]\n({ff_raw})')
                type_error.append(f'[{filename}]: {ff_raw}')

        if attribute_error:
            print('\n' * 5)
            print(('\n' + '-' * 100 + '\n').join(attribute_error))
            print('Attribute error: FAILED, LOOK AT THE ABOVE EXAMPLES!')
            continue

        if type_error:
            print('\n' * 5)
            print(('\n' + '-' * 100 + '\n').join(type_error))
            print('Type error: FAILED, LOOK AT THE ABOVE EXAMPLES!')
            continue


def write_ffs(ffwriter, qw, aprox_files):
    ffwriter.writeheader()
    written = 0
    while ff := qw.get():
        if ff == 'STOP':
            return
        ffwriter.writerow(ff)
        written += 1
        print(f'\rwritten {written:5} of approximately {aprox_files} ffs', end='')


def parse_range(args):
    q = Queue()
    for i in range(args.range_left, args.range_right):
        if os.path.exists(f'html/ao3_{i:04}.html'):
            q.put(i)
    with open(args.output, 'w', newline='') as csvout:
        columns = ['id', 'title', 'author', 'fandoms', 'req_tags', 'last_updated',
                   'tags_commas', 'summary', 'words', 'chapters_written', 'chapters_of',
                   'collections', 'comments', 'kudos', 'bookmarks', 'hits']
        ffwriter = csv.DictWriter(csvout, fieldnames=columns, delimiter='\t')
        qw = Queue()
        ps = [Process(target=parse, args=(q,qw)) for i in range(args.num)]
        writer = Process(target=write_ffs, args=(ffwriter, qw, (args.range_right-args.range_left)*20))
        for p in ps:
            p.start()
        writer.start()
        for p in ps:
            p.join()
        qw.put('STOP')
        writer.join()



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--num', '-n', type=int, help="Number of processes", default=7)
    parser.add_argument('--range-left', '-l', type=int, help="Range start", default=0)
    parser.add_argument('--range-right', '-r', type=int, help="Range end", default=3217)
    parser.add_argument('--output', '-o', help="Output file ", default='ffs.csv')
    args = parser.parse_args()
    parse_range(args)
    print(f"\nDone [{args.range_left:4}, {args.range_right:4})")
