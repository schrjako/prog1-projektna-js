#!/bin/sh

grep "      by" -A 3 html/* | grep -v author | grep Anonymous | uniq -c | sed -e 's| *\([0-9]\+\) html/ao3_\([0-9]\{4\}\).html- *Anonymous|\2: \1|'
