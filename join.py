import csv

ffs = []

for i in range(3217):
    with open(f'data/{i:04}.csv') as f:
        reader = csv.DictReader(f, delimiter='\t', dialect=csv.unix_dialect)
        ffs.extend([row for row in reader])

with open(f'ffs.csv', 'w', newline='') as f:
    csv_writer = csv.DictWriter(f, ffs[0].keys(), delimiter='\t', dialect=csv.unix_dialect)
    csv_writer.writeheader()
    for ff in ffs:
        csv_writer.writerow(ff)
